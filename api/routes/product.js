const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const upload = multer({ storage: storage });

const Product = require('../model/product');
const globalValues = require('../../config');

const productsLink = `${globalValues.base_api_url}/products`;

router.get('/', (req, res, next) => {
    Product.find()
        .select('-__v')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                products: docs.map(doc => {
                    return {
                        name: doc.name,
                        price: doc.price,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: `${productsLink}/${doc._id}`
                        }
                    }
                })
            }
            res.status(200).json(response);
        })
        .catch(error => {
            res.status(500).json(error);
        })
});

router.post('/', upload.single('productImage'), (req, res, next) => {
    const product = new Product({
        _id: mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    });
    product.save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Product created successfully',
                createdProduct: {
                    name: result.name,
                    price: result.price,
                    _id: result._id,
                    request: {
                        type: 'GET',
                        url: `${productsLink}/${result._id}`
                    }
                }
            });
        }).
        catch(error => {
            res.status(500).json({
                error
            });
            console.log(error)
        });

});

router.get('/:productID', (req, res, next) => {
    const id = req.params.productID;
    Product.findById(id)
        .select('-__v')
        .exec()
        .then(doc => {
            console.log(doc);
            if (doc) {
                res.status(200).json({
                    product: doc,
                    request: {
                        type: 'GET',
                        url: `${productsLink}/${id}`
                    }
                });
            } else {
                res.status(404).json({ message: 'No product found with given id.' });
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({ error });
        });
});

router.patch('/:productID', (req, res, next) => {
    const id = req.params.productID;
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Product.updateOne({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Product updated',
                request: {
                    type: 'GET',
                    url: `${productsLink}/${id}`
                }
            });
        })
        .catch(error => {
            console.log(error);
            res.status(500).json(error);
        });
});

router.delete('/:productID', (req, res, next) => {
    const id = req.params.productID;
    Product.deleteOne({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Product Deleted',
                request: {
                    type: 'POST',
                    url: `${productsLink}/${id}`,
                    body: { name: 'String', price: 'Number' }
                }
            });
        })
        .catch(error => {
            res.status(500).json(error);
        });
});

module.exports = router;