const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../model/order');
const Product = require('../model/product');
const globalValues = require('../../config');

const ordersLink = `${globalValues.base_api_url}/orders`;

router.get('/', (req, res, next) => {
    Order.find()
        .select('-__v')
        .populate('product', 'name')
        .exec()
        .then(doc => {
            res.status(200).json({
                count: doc.length,
                orders: doc,
                request: {
                    type: 'GET',
                    url: ordersLink
                }
            });
        })
        .catch(error => {
            res.status(404).json({
                error
            });
        });
});

router.post('/', (req, res, next) => {
    Product.findById(req.body.productID).then(result => {
        const order = new Order({
            _id: mongoose.Types.ObjectId(),
            quantity: req.body.quantity,
            product: req.body.productID
        })

        return order.save()
            .then(product => {
                if (!product) {
                    return res.status(404).json({
                        message: 'Product not found with given id.'
                    });
                }
                res.status(201).json({
                    message: 'Order Placed successfully',
                    placedOrder: {
                        _id: product._id,
                        product: product.product,
                        quantity: product.quantity
                    }
                });
            }).catch(error => {
                res.status(500).json({
                    message: 'Something went wrong',
                    error
                });
            });
    })
        .catch(error => {
            res.status(500).json({
                message: 'There is no product with the given id',
                error
            })
        })

});

router.get('/:orderID', (req, res, next) => {
    const id = req.params.orderID;
    Order.findById(id)
        .select('-__v')
        .populate('product')
        .exec()
        .then(doc => {
            if (doc) {
                res.status(200).json({
                    order: doc,
                    request: {
                        type: 'GET',
                        url: `${ordersLink}/${id}`
                    }
                });
            } else {
                res.status(404).json({ message: 'No order found with given id.' });
            }
        }).catch(error => {
            res.status(500).json({ message: 'Something went wrong', error });
        });
});

// router.patch('/:orderID', (req, res, next) => {
//     res.status(200).json({
//         message: 'Order updated.',
//         id: req.params.orderID
//     });
// });

router.delete('/:orderID', (req, res, next) => {
    Order.remove({ _id: req.params.orderID })
        .exec()
        .then(order => {
            if (!order) {
                return res.status(404).json({
                    message: 'No order found with the given id.'
                });
            }
            res.status(200).json({
                order,
                message: 'Order deleted',
                request: {
                    type: 'POST',
                    url: `${ordersLink}`,
                    body: { productID: 'ID', quantity: "Number" }
                }
            });
        })
        .catch(error => {
            res.status(500).json({
                message: 'Something wrong happened',
                error
            });
        });
});

module.exports = router;